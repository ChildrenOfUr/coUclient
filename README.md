# `coUclient`

Children of Ur's Dart-based browser client

This repository contains the source code for Children of Ur's Dart-based browser client.

[![pipeline status](https://gitlab.com/ChildrenOfUr/coUclient/badges/dev/pipeline.svg)](https://gitlab.com/ChildrenOfUr/coUclient/commits/dev)
[![Netlify Status](https://api.netlify.com/api/v1/badges/bca45019-b6ba-49c8-bfd4-a70adc6032ab/deploy-status)](https://app.netlify.com/sites/cou-client/deploys)

[![Deploys by Netlify](https://www.netlify.com/img/global/badges/netlify-color-bg.svg)](https://www.netlify.com/)

## License

Children of Ur is based on Tiny Speck's browser-based game, Glitch&trade;. The original game's elements have been released into the public domain.
For more information on the original game and its licensing information, visit <a href="http://www.glitchthegame.com" target="_blank">glitchthegame.com</a>.

License information for other assets used in Children of Ur can be found in [ATTRIBUTION.md](ATTRIBUTION.md).

## Usage

The code is live at [game.childrenofur.com](https://game.childrenofur.com).

If you want to run it locally or on your own server, you'll need to have an environment with [Dart](https://www.dartlang.org/) installed. Note that this repository does not currently contain any prebuilt files, so you'll also need a development environment. See [CONTRIBUTING.md](CONTRIBUTING.md).

## General Roadmap

The project is built in [Dart](https://www.dartlang.org),
which is then compiled and minified into javascript and can run in most browsers. See our team collaboration
site on Trello for the current roadmap.
