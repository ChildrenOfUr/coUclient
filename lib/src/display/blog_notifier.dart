part of couclient;

class BlogNotifier {
	static const _LS_KEY = "cou_blog_post";
	static const _URL = "https://www2.childrenofur.com/api/latest_blog_post";

	static String get _lastSaved {
		if (localStorage[_LS_KEY] != null) {
			return localStorage[_LS_KEY];
		} else {
			return "";
		}
	}

	static set _lastSaved(dynamic id) {
		localStorage[_LS_KEY] = id.toString();
	}

	static void _notify(String link, String title) {
		new Notification(
			"Children of Ur Blog",
			body: "Click here to read the new post: \n$title",
			icon: Toast.notifIconUrl
		)..onClick.listen((_) {
			window.open(link, "_blank");
		});
	}

	static Future<void> refresh() async {
		try {
			String json = await HttpRequest.getString(_URL);
			Map<String, dynamic> map = jsonDecode(json);

			if (!map.containsKey("error")) {
				String id = map["id"].toString();
				String title = map["title"];
				String url = map["url"];

				// Compare to last known post
				if (id != _lastSaved) {
					// New post
					_notify(url, title);

					// Update cached
					_lastSaved = id;
				}
			} else {
				logmessage("Error getting latest blog post: ${map["error"]}");
			}
		} catch (_) {
			// Possibly blocked by ad blocker?
		}
	}
}
