part of couclient;

class EmoticonPicker extends Modal {
	static final String emoticonUrl = "/files/emoticons";
	static final String emoticonType = "png";
	static final RegExp emoticonRegex = RegExp(":([a-z0-9_]*):", caseSensitive: false);
	static Map<String, List<String>> emoji = {};
	static List<String> emojiNames = [];

	String id = 'emoticonPicker';
	Element window, well, grid;
	InputElement search;
	InputElement target;

	EmoticonPicker() {
		window = querySelector("#$id");
		well = window.querySelector("#emoticonPicker .well");
		search = well.querySelector("#ep-search");
		grid = well.querySelector("#ep-grid");

		prepare();

		HttpRequest.getString(emoticonUrl + "/emoticons.json").then((String json) {
			// Load categorical listing
			emoji.clear();
			Map<String, List<dynamic>>.from(jsonDecode(json))
				.forEach((String key, List<dynamic> list) => emoji[key] = list.cast<String>());

			// Flatten
			emojiNames.clear();
			for (List<String> category in emoji.values) {
				emojiNames.addAll(category);
			}
		});

		search.onInput.listen((_) {
			displayEmoticons(search.value);
		});
	}

	void displayEmoticons([String search = null]) {
		grid.children.clear();

		emoji.forEach((String category, List<String> emoticons) {
			HeadingElement categoryTitle = HeadingElement.h2()
				..text = category;

			grid.append(categoryTitle);

			for (String emoticon in emoticons) {
				if (search != null && !emoticon.contains(search)) {
					continue;
				}

				ImageElement emoticonImage = ImageElement()
					..src = "$emoticonUrl/$emoticonType/$emoticon.$emoticonType"
					..classes.addAll(["emoticon"])
					..draggable = true;

				SpanElement emoticonButton = SpanElement()
					..classes.add("ep-emoticonButton")
					..title = emoticon
					..dataset['emoticon-name'] = emoticon
					..append(emoticonImage);

				grid.append(emoticonButton);

				emoticonButton.onDragStart.listen((e) {
					e.stopPropagation();
					e.dataTransfer.effectAllowed = "copy";
					e.dataTransfer.setData("text/plain", ":${emoticon}:");
					e.dataTransfer.setDragImage(emoticonImage, emoticonImage.clientWidth ~/ 2, -10);
				});

				emoticonButton.onClick.listen((_) {
					bool endsWithSpace = target.value == "" || target.value.substring(target.value.length - 1) == " ";
					target.value += "${endsWithSpace ? "" : " "}:${emoticon}:";
				});
			}
		});
	}

	@override
	open({bool ignoreKeys: false, String title: null, Element input: null}) {
		this.target = input;

		// Update window text
		if (title.toLowerCase().contains("chat")) {
			querySelector("#ep-channelname").text = title;
		} else {
			querySelector("#ep-channelname").text = "chat with $title";
		}

		displayEmoticons();
		displayElement.hidden = false;
		search.focus();
		elementOpen = true;
	}

	static String parse(String text) =>
		text.replaceAllMapped(emoticonRegex, (Match m) {
			if (emojiNames.contains(m[1])) {
				return '<img class="emoticon" title="${m[1]}" src="$emoticonUrl/$emoticonType/${m[1]}.$emoticonType">';
			} else {
				return m[0];
			}
		});
}
