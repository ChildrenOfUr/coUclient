part of couclient;

bool showCollisionLines = false;
DateTime now, lastUpdate = DateTime.now();

// Our renderloop
render() {
	now = DateTime.now();

	if (serverDown) {
		return;
	}

	//Draw Street
	if (currentStreet is Street)
		currentStreet.render();
	//Draw Player
	if (CurrentPlayer is Player)
		CurrentPlayer.render();

	//draw quoins
	quoins.forEach((String id, Quoin quoin) => quoin.render());

	//draw entites
	entities.forEach((String id, Entity entity) => entity.render());

	//draw other players
	otherPlayers.forEach((String name, Player player) => player.render());

	//update minimap
	minimap.updateObjects();

	// update gps
	gpsIndicator.update();

	if (showCollisionLines) {
		showPlayerRect();
	}

	lastUpdate = now;
}
