library configs;

import 'dart:html';
import 'dart:async';
import 'dart:convert';

class Configs {
	static final String prodBaseAddress = 'server.childrenofur.com';

	static String gameServerAddress;
  static String authServerAddress;
	static String utilServerAddress;
	static String websocketServerAddress;
	static String authAddress;
	static String authWebsocket;

	static String http = gameServerAddress != "localhost" ? 'https:' : 'http:';
	static String ws = gameServerAddress != "localhost" ? 'wss:' : 'ws:';
	static bool testing;

	static final int clientVersion = int.parse(
		Uri.parse(querySelector('#dart-src').attributes["src"])
		.queryParameters["clientVersion"]);

	static Future init() async {
    HttpRequest data = await HttpRequest.request('server_domain.json');
    print(data.response);
		Map addresses = jsonDecode(data.response);
    gameServerAddress = addresses['gameServer'];
    authServerAddress = addresses['authServer'];
    String gameUtilPort = addresses['gameUtilPort'];
    String gameWebsocketPort = addresses['gameWebsocketPort'];
		utilServerAddress = '$gameServerAddress:$gameUtilPort';
		websocketServerAddress = '$gameServerAddress:$gameWebsocketPort';
		authAddress = '$authAddress:8383';
		authWebsocket = '$authAddress:8484';
		testing = gameServerAddress != prodBaseAddress;

		// TODO auction-house
//		Element auctionHouse = querySelector('auction-house');
//		auctionHouse.attributes['serverAddress'] = '$http//$utilServerAddress';
	}

	static String proxyStreetImage(String url) {
		return _proxyImage('streets', url);
	}

	static String proxyAvatarImage(String url) {
		return _proxyImage('avatars', url);
	}

	static String proxyMapImage(String url) {
		return _proxyImage('maps', url);
	}

	static String _proxyImage(String type, String url) {
		if (type == null || url == null) {
			return null;
		}

		return 'https://childrenofur.com/assets/$type/?url=$url';
	}
}
